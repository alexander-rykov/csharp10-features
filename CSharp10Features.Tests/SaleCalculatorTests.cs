﻿namespace CSharp10Features.Tests
{
    [TestFixture]
    public class SaleCalculatorTests
    {
        [TestCase("WA", 1.0, ExpectedResult = 0.06)]
        [TestCase("WA", 2.0, ExpectedResult = 0.12)]
        [TestCase("MN", 1.0, ExpectedResult = 0.075)]
        [TestCase("MN", 2.0, ExpectedResult = 0.15)]
        [TestCase("MI", 1.0, ExpectedResult = 0.05)]
        [TestCase("MI", 2.0, ExpectedResult = 0.1)]
        [TestCase("AM", 1.0, ExpectedResult = 0.0)]
        [TestCase("AM", 2.0, ExpectedResult = 0.0)]
        public double ComputeSalesTax_ReturnsTax(string state, double salePrice)
        {
            var address = new Address
            {
                State = state,
            };

            return SaleCalculator.ComputeSalesTax(address, salePrice);
        }
    }
}
