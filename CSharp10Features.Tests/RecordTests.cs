﻿global using NUnit.Framework;

namespace CSharp10Features.Tests
{
    [TestFixture]
    public class RecordTests
    {
        [Test]
        public void TestProperties()
        {
            var r = new Records(10, "Hello, world!", true);

            Assert.AreEqual(10, r.I);
            Assert.AreEqual("Hello, world!", r.S);
            Assert.AreEqual(true, r.B);
        }

        [Test]
        public void TestDeconstruct()
        {
            var r = new Records(101, "Bla bla bla", true);

            int i;
            (i, string s, bool b) = r;

            Assert.AreEqual(101, i);
            Assert.AreEqual("Bla bla bla", s);
            Assert.AreEqual(true, b);
        }

        [Test]
        public void TestCompare()
        {
            var r1 = new Records(5783, "Lorem ipsum", true);
            var r2 = new Records(5783, "Lorem ipsum", true);
            var r3 = new Records(9382, "Ipsum lorem", false);

            Assert.IsTrue(r1 == r2);
            Assert.IsFalse(r1 == r3);
        }
    }
}
