﻿namespace CSharp10Features.Tests
{
    [TestFixture]
    public class PatternMatchingTests
    {
        [TestCase('a', ExpectedResult = true)]
        [TestCase('z', ExpectedResult = true)]
        [TestCase('A', ExpectedResult = true)]
        [TestCase('B', ExpectedResult = true)]
        [TestCase('.', ExpectedResult = true)]
        [TestCase(',', ExpectedResult = true)]
        [TestCase(' ', ExpectedResult = false)]
        [TestCase(';', ExpectedResult = false)]
        [TestCase('-', ExpectedResult = false)]
        public bool IsLetterOrSeparator_ReturnsBooleanValue(char c)
        {
            return PatternMatching.IsLetterOrSeparator(c);
        }
    }
}
