﻿namespace CSharp10Features
{
    public static class PatternMatching
    {
        // 2 SA1008 issues.
        public static bool IsLetterOrSeparator(this char c) =>
            c is (>= 'a' and <= 'z') or (>= 'A' and <= 'Z') or '.' or ',';
    }
}
