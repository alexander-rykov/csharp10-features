﻿namespace CSharp10Features
{
    // 3 SA1313 issues.
    public record Records(int I, string S, bool B);
}
