﻿namespace CSharp10Features
{
    public static class SaleCalculator
    {
        public static double ComputeSalesTax(Address location, double salePrice)
        {
            return location switch
            {
                { State: "WA" } => salePrice * 0.06,
                { State: "MN" } => salePrice * 0.075,
                { State: "MI" } => salePrice * 0.05,
                _ => 0
            };
        }
    }

    public class Address
    {
        public string? State { get; init; }
    }
}
