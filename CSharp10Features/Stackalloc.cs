﻿namespace CSharp10Features
{
    public class Stackalloc
    {
        public int GetNumberOfElements()
        {
            // 2 SA1500 issues.
            Span<int> numbers = stackalloc[] { 1, 2, 3, 4, 5, 6 };
            return numbers.IndexOfAny(stackalloc[] { 2, 4, 5, 9 });
        }
    }
}
