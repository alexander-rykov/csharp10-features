# csharp10-features

C# 10
* Global namespaces.
* Assignment and declaration in same deconstruction.

C# 9
* Records.
* Init only setters.
* Pattern matching enhancements.

C# 8
* Property patterns.
* Stackalloc in nested expressions.
